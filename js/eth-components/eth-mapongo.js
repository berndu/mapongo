/**
* @memberof eth
* @ngdoc component
* @name ethMapongo
*
* @description
* Eth Component:<br>
* - Link, map and QRCode for mapongo will be added
*
* <b>Requirements</b><br>
*
* Service {@link eth.mapongoLibrariesService}<br>
*
* <b>Used by</b><br>
*
* Eth After Component {@link eth.prmLocationItemsAfter}<br>
*
*
* <b>In Package</b><br>
*
* DADS, NEBIS<br>
 */

app.component('ethMapongo', {
    bindings: {
        parentCtrl: '<',
        recordid: '@',
        callnumber: '@',
        barcode: '@',
        sublocation: '@'
    },
    template: `<div class="eth-mapongo" layout="row" layout-sm="column" layout-xs="column">
                    <div class="eth-mapongo-qrc">
                        <img alt="QR Code" src="{{$ctrl.getMapongoQRC()}}"/>
                        <div translate="nui.customizing.mapongoqrc"></div>
                    </div>
                    <div class="eth-mapongo-link">
                        <a target="_blank" href="{{$ctrl.getMapongoUrl()}}">
                            <img alt="{{(\'nui.customizing.mapongoalt\' | translate)}}" src="{{$ctrl.getMapongoImageSrc()}}"/>
                        </a>
                    </div>
                </div>`,

    controller: class EthMapongoController {

        constructor(mapongoLibrariesService) {
            this.mapongoLibrariesService = mapongoLibrariesService;
            this.lang = 'de';
        }

        $onInit() {
            if (!this.parentCtrl.userSessionManagerService || !this.parentCtrl.userSessionManagerService.$state) {
                console.error("***ETH*** EthMapongoController.$onInit: userSessionManagerService or $state not available");
                this.lang = 'de';
            }
            else{
                // window.appConfig.primo-view.attributes-map.interfaceLanguage
                this.lang = this.parentCtrl.userSessionManagerService.$state.params.lang.substr(0,2);
            }
        };

        getMapongoImageSrc() {
            let src = "";
            try{
                if (!this.parentCtrl.loc || !this.parentCtrl.loc.location) {
                    console.error("***ETH*** EthMapongoController.getMapongoImageSrc: loc or loc.location not available");
                    return;
                }
                let loc = this.parentCtrl.loc.location;
                let baseUrl = this.mapongoLibrariesService.getBaseUrl(loc.libraryCode);

                src = baseUrl + "/static_images/projects/1/search_thumbnail.jpg?project_id=1&search_key=" + encodeURIComponent(this.callnumber);
                if (loc.libraryCode === "E51" || loc.libraryCode === "UPHZH" ) {
                    src += "&search_context2=" + encodeURIComponent(this.sublocation);
                }
                if (loc.libraryCode === "E65") {
                    src += "&e=" + encodeURIComponent(this.recordid);
                }
            }
            catch(e){
                console.error("***ETH*** EthMapongoController.getMapongoImageSrc:");
                console.error(e.message);
            }
            return src;
        }

        getMapongoUrl() {
            let url = "";
            try{
                let loc = this.parentCtrl.loc.location;
                let baseUrl = this.mapongoLibrariesService.getBaseUrl(loc.libraryCode);

                url = baseUrl + "/viewer?project_id=1&search_key=" + encodeURIComponent(this.callnumber) + "&language=" + this.lang + "&e=" + encodeURIComponent(this.recordid);
                // ZHAW-HSB-WIN
                if (loc.libraryCode === "E51" || loc.libraryCode === "UPHZH" ) {
                    url += "&search_context2=" + encodeURIComponent(this.sublocation);
                }
                if (this.barcode != "") {
                        url += "&bcd=" + this.barcode;
                }

            }
            catch(e){
                console.error("***ETH*** EthMapongoController.getMapongoUrl:");
                console.error(e.message);
            }
            return url;
        }

        getMapongoQRC() {
            let src = "";
            try{
                let loc = this.parentCtrl.loc.location;
                let baseUrl = this.mapongoLibrariesService.getBaseUrl(loc.libraryCode);

                src = baseUrl + "/static_images/projects/1/search_qrcode.png?project_id=1&search_key=" + encodeURIComponent(this.callnumber) + "&language=" + this.lang + "&e=" + encodeURIComponent(this.recordid);
                if (loc.libraryCode === "E51" || loc.libraryCode === "UPHZH" ) {
                    src += "&search_context2=" + encodeURIComponent(this.sublocation);
                }
                if (this.barcode != "") {
                    src += "&bcd=" + this.barcode;
                }
            }
            catch(e){
                console.error("***ETH*** EthMapongoController.getMapongoQRC:");
                console.error(e.message);
            }
            return src;
        }

    }
});
