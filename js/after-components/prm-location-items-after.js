/**
* @memberof eth
* @ngdoc directive
* @name prmLocationItemsAfter
*
* @description
* ETH After Component<br>
*
* Customization for the location items:<br>
* - Link, map and QRC for Mapongo are added.<br>
*
* If there is 1 location: prmLocationItemsAfter
*
* If there are multiple locations: prmLocationAfter;
* after all locations: prmLocationsAfter;
* after choosing a location: prmLocationItemsAfter
*
* <b>Requirements</b><br>
*
* Service {@link eth.mapongoLibrariesService}<br>
*
* Eth Component {@link eth.ethMapongo}
*
*/
app.component('prmLocationItemsAfter', {
    bindings: { parentCtrl: '<' },
    controller: class LocationItemsAfterController {
        constructor($scope, $compile, mapongoLibrariesService) {
            this.$scope = $scope;
            this.$compile = $compile;
            this.mapongoLibrariesService = mapongoLibrariesService;
            this.oldLoc = null;
        }
/**
* @ngdoc method
* @name prmLocationItemsAfter#$doCheck
* @eventOf prmLocationItemsAfter
* @description
* - checks if the current location is registered for Mapongo
* - eventually injects an eth-mapongo directive and specific attributes for it
*/
        $doCheck() {
            try{
                if (!this.parentCtrl.loc || !this.parentCtrl.loc.location || !this.parentCtrl.loc.items || this.parentCtrl.loc.items.length == 0) {
                    return;
                }
                if (!this.mapongoLibrariesService.isMapongoLocation(this.parentCtrl.loc.location.libraryCode, this.parentCtrl.loc.location.subLocationCode)) {
                    return;
                }
                let aLoc = document.querySelectorAll('prm-location-items .md-list-item-text');
                if (aLoc.length < 1) {
                    return;
                }
                if (angular.equals(this.oldLoc, this.parentCtrl.loc)) {
                    return;
                }
                this.oldLoc = this.parentCtrl.loc;

                // Example:  Bildsprache [zwei] 2 : Lehrbuch für den Fachbereich Bildende Kunst : visuelle Kommunikation in der Sekundarstuffe
                this.callnumber = "";
                this.barcode = "";
                this.subLocationCode = "";
                this.recordid = this.parentCtrl.item.pnx.control.recordid;
                // iterate items of a location (Exemplare), get reference to html container of this item and inject etm-mapongo-directive
                for(let i = 0; i<this.parentCtrl.loc.items.length; i++){
                    if (aLoc[i].children[0]) {
                        let item = this.parentCtrl.loc.items[i];
                        let locColChildren = aLoc[i].children[0].children;
                        // Nodelist to Array
                        let aLocColChildren = Array.prototype.slice.call(locColChildren);
                        // is mapongo Container already appended?
                        let aMapongoChildren = aLocColChildren.filter(function(o) {return o.className === 'eth-mapongo-container';} );
                        if (aMapongoChildren.length < 1) {
                            if (!this.mapongoLibrariesService.isMapongoSubLocation(this.parentCtrl.loc.location.libraryCode, item._additionalData.secondarylocationname)) {
                                continue;
                            }
                            if (item._additionalData.itemcategoryname === "Online") {
                                continue;
                            }
                            if (item._additionalData.itemstatusname !== "") {
                                continue;
                            }

                            this.subLocationCode = this.mapongoLibrariesService.getSubLocationCode(this.parentCtrl.loc.location.libraryCode, item._additionalData.secondarylocationname);
                            this.callnumber = item._additionalData.callnumber;
                            let barcodeEntry = item.fullItemFields.filter(function(e) {return e.indexOf("Barcode:") > -1 || e.indexOf("Strichcode:") > -1;} );
                            this.barcode = "";
                            if (barcodeEntry.length > 0) {
                                this.barcode = barcodeEntry[0].replace("Barcode: ","").replace("Strichcode: ","");
                            }
                            angular.element(aLoc[i].children[0]).append(this.$compile('<eth-mapongo class="eth-mapongo-container" recordid="' + this.recordid + '" barcode="' + this.barcode + '" callnumber="' + this.callnumber + '" sublocation="' + this.subLocationCode + '"parent-ctrl="$ctrl.parentCtrl"></eth-mapongo>')(this.$scope));
                        }
                    }
                }
            }
            catch(e){
                console.error("***ETH*** LocationItemsAfterController.$doCheck:");
                console.error(e.message);
            }
        }
    }
});
