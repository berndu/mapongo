/**
* @memberof eth
* @ngdoc service
* @name mapongoLibrariesService
*
* @description
* Holds informations about the libraries which use mapongo
*
* <b>Used by</b><br>
*
* Eth Component {@link eth.ethMapongo}<br>
*
*
* <b>In Package</b><br>
*
* DADS, NEBIS<br>
 */
app.factory('mapongoLibrariesService', function() {
    let libraries = [
            {
                    "shortname": "ZHdK-MIZ",
                    "name": "ZHdK-MIZ",
                    "libraryCode": "E65",
                    "baseUrl": "https://zhdk.mapongo.de",
                    "locationCodeWhiteList": ["E65G1","E65G2","E65E1","E65U1","E65U4"],
                    "locationWhiteList": ["Galeriegeschoss","Eingangsgeschoss","Untergeschoss","Untergeschoss Curating Degree Zero Archive"]
            },
            {
                    "shortname": "ZHAW-HSB-ZH",
                    "name": "ZHAW Hochschulbibliothek Zürich",
                    "libraryCode": "E97",
                    "baseUrl": "https://zhdk.mapongo.de",
                    "locationCodeWhiteList": ["E97E1"],
                    "locationWhiteList": ["Eingangsgeschoss"]
            },
            {
                    "shortname": "ZHAW-HSB-WIN",
                    "name": "ZHAW Hochschulbibliothek Winterthur",
                    "libraryCode": "E51",
                    "baseUrl": "https://zhaw.mapongo.de",
                    "locationCodeWhiteList": ["E51B0","E51B9","E51B2","E51B8","E51BN","E51BL","E51BG","E51BT","E51BB","E51BW","E51BA","E51BK","E51BR"],
                    "locationWhiteList": ["Ausstellung Neuerwerbungen - Eingangsgeschoss 0","Handapparate - Eingangsgeschoss 0","Compactus, offen - Eingangsgeschoss 0","Zeitschriften - Eingangsgeschoss 0","AN, Allgemeines und Nachschlagewerke - Eingangsgeschoss 0","AL, Angewandte Linguistik - Eingangsgeschoss 0","GM, Gesundheit und Medizin - Eingangsgeschoss 0","TN, Technik und Naturwissenschaften - Zwischengeschoss 0.1","WB, Wissenschaft und Bildung - Eingangsgeschoss 0","WM, Wirtschaft und Management - Zwischengeschoss 0.1","AB, Architektur und Bau- Zwischengeschoss 0.1","KU, Kunst und Unterhaltung - Eingangsgeschoss 0","RP, Recht und Politik - Zwischengeschoss 0.2"]
            },
            {
                    "shortname": "PHZH",
                    "name": "PH Zürich, Bibliothek",
                    "libraryCode": "UPHZH",
                    "baseUrl": "https://phzh.mapongo.de",
                    "locationCodeWhiteList": ["ULFHF","ULFHG","ULFHN","ULFHS"],
                    "locationWhiteList": ["Stockwerk F - frei zugänglich","Stockwerk G - frei zugänglich", "Stockwerk H Nord - frei zugänglich","Stockwerk H Süd - frei zugänglich"]
            },
            {
                    "shortname": "HGK-MED",
                    "name": "FHNW HGK-Mediathek Basel",
                    "libraryCode": "E75",
                    "baseUrl":"https://mediathek.hgk.fhnw.ch/nebis",
                    "locationCodeWhiteList": ["","HAP"],
		            "locationWhiteList":["", "Handapparat"]
            },
            {
                    "shortname": "HGK-MED",
                    "name": "FHNW Campus Muttenz Bibliothek",
                    "libraryCode": "E44",
                    "baseUrl":"https://fhnw-cmu.mapongo.de",
                    "locationCodeWhiteList": [""],
                    "locationWhiteList":[""]
            }
    ]

    function getMapongoLibrary(libraryCode){
      let filterLibraries = libraries.filter(function(l) {return l.libraryCode === libraryCode;} );
      if (filterLibraries.length > 0)
          return filterLibraries[0];
      else
          return null;
    }

    function isMapongoLocation(libraryCode, locationCode){
      let filterLibraries = libraries.filter(function(l) {return l.libraryCode === libraryCode} );
      if (filterLibraries.length > 0)return true;
      else return false;
    }

    function isMapongoSubLocation(libraryCode, subLocation){
      let filterLibraries = libraries.filter(function(l) {return l.libraryCode === libraryCode && l.locationWhiteList.indexOf(subLocation)>-1;} );
      if (filterLibraries.length > 0)return true;
      else return false;
    }

    function getSubLocationCode(libraryCode, subLocation){
        let filterLibraries = libraries.filter(function(l) {return l.libraryCode === libraryCode} );
        if (filterLibraries.length === 0)return null;
        let i = filterLibraries[0].locationWhiteList.indexOf(subLocation);
        return filterLibraries[0].locationCodeWhiteList[i];
    }

    function getBaseUrl(libraryCode){
        let lib = getMapongoLibrary(libraryCode);
        if (lib) {
            return lib.baseUrl;
        }
        else{
            return "";
        }

    }

    return {
        isMapongoLocation: isMapongoLocation,
        isMapongoSubLocation: isMapongoSubLocation,
        getBaseUrl: getBaseUrl,
        getSubLocationCode: getSubLocationCode
    };
});
